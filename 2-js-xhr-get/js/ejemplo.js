(() => { // NOTA: Es una buena práctica utilizar IIFE (Immediately Invoked Function Expression) para no contaminar el objeto global. Ver https://flaviocopes.com/javascript-iife 

    var btnGet = document.getElementById("btnGet");

    btnGet.onclick = () => {
        var url = document.getElementById("url").value;
        let request = new XMLHttpRequest();
        request.open('GET', url)
        request.onload = () => {
            let resultadoSection = document.getElementById("resultado");
            resultadoSection.innerHTML = request.responseText;
        }
    
        request.send();
    };
    
})();